# Bash evaluation
This was my evaluation for Bash at MyDigitalSchool - Paris.

Author : Florian LINA

## Evaluation : My Magic Prompt

To acces the prompt menu you have to connect yourself via login name and a password. The projct must be in a directory named ~/my-magic-prompt/

This are the login you need in order to acces the prompt and its commands

Login : flo
Password : plop


This prompt respond commands :

| Fonction | Description|Argument
|:---:|:---|---:|
|`help` / `-h`|indicate all possible command and what they do|no|
|`ls`|list of all files and directory hiden or not|no|
|`rm`|delete a file|yes|
|`rmd` / `rmdir`|delete a directory|yes|
|`about`|a quick description of the program  |no|
|`version` / `--v` / `vers`|show the version of this prompt|no|
|`age`|ask for your age and tell you if you'r a minor and if so kicks you out|no|
|`profile`|show different information about you|no|
|`passw`|allow you to change your password after confirmation|no|
|`cd`|go to file or directory of your choice or to go back one before|yes|
|`pwd`|show current repository|no|
|`hour`|show you the current time|no|
|`httpget`|allow you to download the html souce code of a web page and save it on a file. The prompt will ask you to nam the file|yes|
|`open`|open a file with VIM even if the file does not already exist|yes|
|`quit`|allow to quit the prompt|no|
|`*`|show up when its an unknown command|no|

## Evaluation BONUS : My Magic Prompt

Ad the command: 

| Fonction | Description|Argument
|:---:|:---|---:|
|`rps`|allow you to play Rock Paper Scissors against a bot|no|
|`rmdirwtf`|allow you to delete one or more files or directory via password confirmation|no|
